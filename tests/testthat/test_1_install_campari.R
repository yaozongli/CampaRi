context('install_campari')

test_that('Test campari original installation from inst directory', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  require(testthat); require(CampaRi); require(tictoc)
  # ------------------- =
  
  # installing (fastly) campari SOMEWHERE
  ev_it(CampaRi::install_campari(install_ncminer = T, install_threads = T, silent_built = silent, no_optimization = T, other_flags = '--with-trailing-user-fcflags=-fPIC'), NA) # to do it usually
  
  # expect_error(CampaRi::install_campari(install_threads = T, install_mpi = T, silent_built = T, no_optimization = T), NA) # to do it usually
  # expect_error(install_campari(install_threads = T, silent_built = F, no_optimization = T), NA)
  # expect_error(install_campari(install_ncminer = T, silent_built = F), NA)
  # expect_error(install_campari(install_threads = T, install_mpi = T, silent_built = F), NA)
  # expect_error(install_campari(install_threads = T, install_mpi = T, silent_built = F), NA)
  ev_it(install_campari(install_ncminer = T, install_threads = T, install_mpi = T, silent_built = silent), err = T)
  ev_it(install_campari(installation_location = 'sadsdasdsadsadsadsadsadsafdfsdf', install_ncminer = T, install_threads = T, install_mpi = T, silent_built = silent), err = T)
  
  if(!dir.exists('to_delete_just_in_a_moment')) dir.create('to_delete_just_in_a_moment')
  ev_it(install_campari(installation_location = 'to_delete_just_in_a_moment', install_ncminer = 'asd', install_threads = F, silent_built = silent), err = T) # this is done from another test
  if(dir.exists('to_delete_just_in_a_moment')) unlink('to_delete_just_in_a_moment', recursive = T)
  
  if(file.exists('Makefile')) file.remove('Makefile')
  if(file.exists('VERSION')) file.remove('VERSION')
  if(file.exists('config.log')) file.remove('config.log')
  if(file.exists('config.status')) file.remove('config.status')
})
