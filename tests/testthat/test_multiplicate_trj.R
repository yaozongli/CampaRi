context('multiplicate_trj')

test_that('multiplicate trj', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  require(testthat); require(CampaRi); require(tictoc)
  # ------------------- =
  
  trj <- matrix(rnorm(1000), nrow = 100, ncol = 10)
  expect_true(!is.null(trj))
  ev_it(multiplic1 <- multiplicate_trj(trj, window = 15), NA)
  ev_it(multiplic1 <- multiplicate_trj(trj, window = 15, method = 'sincos'), NA)
  ev_it(multiplic1 <- multiplicate_trj(trj, window = 3, spacing_window = 10), NA)
})






