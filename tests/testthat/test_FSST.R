context('FSST')

test_that('Forest of Short Spanning Trees', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  require(testthat); require(CampaRi); require(tictoc); require(ggplot2)
  # ------------------- =
  
  # creation of the dataset ======================================================================================================
  n_elem <- 1000; n_feats <- 2; n_states <- 7; n_shifts <- 20; min_shift <- 25; noise_perc <- 1/10; n_noise <- round(n_elem*noise_perc)
  range_space_m <- 1:25; range_space_sd <- 1
  shifts_times <- array(0, dim = 2)
  base_name <- paste0("trj", n_feats, "feats_", n_elem, "leng")
  # find the shifts
  while(any(diff(shifts_times) < min_shift) || length(shifts_times) <= n_shifts) {
    shifts_times <- c(1, sort(sample(1:n_elem, size = n_shifts + round(n_shifts/10), replace = F)), n_elem + 1) # + 1 is for the next loop
    shifts_times <- shifts_times[ -which(diff(shifts_times) < min_shift)]
    if(!silent) cat(length(shifts_times), "\n")
  }
  
  # ids of the shifts
  ids_shifts <- sample(1:n_states, length(shifts_times) - 1, replace = T)
  
  trj <- array(0.0, dim = c(n_elem, n_feats))
  ann <- array(0, dim = n_elem)
  cl_m <- sample(range_space_m, n_states) # random
  cl_sd <- sample(range_space_sd, n_states, replace = T) # all 1
  
  cl_m <- c(1, 2, 6, 9, 12, 20, 20.3) # first two overlapping and different diffusion, # second group of three with 3 diff and 2 stddev, # third group with overlap
  cl_sd <- c(0.2, 2, 2 , 2, 2, 1, 1)
  
  # gaussian points in 3D space 
  for(i in 2:length(shifts_times)){
    for(j in 1:n_feats)
      trj[shifts_times[i-1]:(shifts_times[i] - 1), j] <- rnorm(diff(shifts_times)[i-1], mean = cl_m[ids_shifts[i-1]], sd = cl_sd[ids_shifts[i-1]])
    if(anyNA(trj)) cat(i)
    ann[shifts_times[i-1]:(shifts_times[i] - 1)] <- rep(ids_shifts[i-1], diff(shifts_times)[i-1])
  } 
  # adding noise
  where_noise <- sample(1:n_elem, size = n_noise)
  for(i in where_noise) trj[i, 1:n_feats] <- rnorm(n_feats, mean = runif(1, min = min(cl_m), max = max(cl_m)), sd = 3)
  ann[where_noise] <- 0
  
  # _plotting dataset ---------------------------------------------------------
  if(plt_stff){
    df_pl <- data.frame("Time" = 1:n_elem, "Where" = trj[,1], "Ann" = as.factor(ann+1))
    gg_data <- ggplot(df_pl, mapping = aes(x=Time, y=Where, col=Ann)) + geom_point() + theme_classic() + 
      scale_color_brewer(palette = "Set1") + theme(legend.title = element_text("State"))
    print(gg_data)
  }
  ### OUTPUT: trj, ann
  
  # full run --------------------------------------------------------------------------------------------------
  ev_it(finalout <- FSST(trj = trj, ann = ann, searchatt = rep(100, 2), unispli = seq(5,100,5), pk_span = 10, ny = 50, sapifaile = "PI_TODELETE.dat", base_name = "TODELETE", hyper_method = "jaccard", 
        leaves.tofold = 0, percs = c(0.9, 0.9), plotting = F, save_plots = T, return_plots = T, list_predi_reso = NULL, only_SSTs = FALSE, skip_matching = F, gl_silent = silent, dbg_FSST = F), s = silent)
  
  # only forest --------------------------------------------------------------------------------------------------
  ev_it(finalout <- FSST(trj = trj, ann = ann, searchatt = rep(100, 2), unispli = seq(5,100,5), pk_span = 10, ny = 50, sapifaile = "PI_TODELETE.dat", base_name = "TODELETE", hyper_method = "jaccard", 
        leaves.tofold = 0, percs = c(0.9, 0.9), plotting = F, save_plots = T, return_plots = T, list_predi_reso = NULL, only_SSTs = T, skip_matching = F, gl_silent = silent, dbg_FSST = F), s = silent)
  
  # skip_matching --------------------------------------------------------------------------------------------------
  ev_it(finalout <- FSST(trj = trj, ann = ann, searchatt = rep(100, 2), unispli = seq(5,100,5), pk_span = 10, ny = 50, sapifaile = "PI_TODELETE.dat", base_name = "TODELETE", hyper_method = "jaccard", 
        leaves.tofold = 0, percs = c(0.9, 0.9), plotting = F, save_plots = T, return_plots = T, list_predi_reso = NULL, only_SSTs = FALSE, skip_matching = T, gl_silent = silent, dbg_FSST = F), s = silent)
  
  # only MCLA and matching --------------------------------------------------------------------------------------------------
  ev_it(finalout <- FSST(trj = trj, ann = ann, sapifaile = "PI_TODELETE.dat", base_name = "TODELETE", hyper_method = "jaccard", 
        leaves.tofold = 0, percs = c(0.9, 0.9), plotting = F, save_plots = T, return_plots = T, list_predi_reso = finalout$list_predi_reso, 
        only_SSTs = T, skip_matching = F, gl_silent = silent, dbg_FSST = F), s = silent)
  
  
  CampaRi::loop_file_rm(list.files(pattern = "TODELETE"))
  })
