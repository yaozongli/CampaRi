dtm <- matrix(rnorm(500), 100, 5)
ah <- CampaRi::distance(dtm)
#adjl <- CampaRi::mst_from_trj(trj = matrix(rnorm(1000), nrow = 100, ncol = 10), dump_to_netcdf = FALSE, birch_clu = T, mute_fortran = F)
#CampaRi::run_campari(trj = dtm, base_name = "todel",
#                           FMCSC_CPROGINDMODE=2, #mst
#                           FMCSC_CCOLLECT=1, 
#                           FMCSC_CMODE=4,
#			   FMCSC_BIRCHHEIGHT=5,
#                           FMCSC_CDISTANCE=7, #rmsd without alignment 7 - dihedral distances need a complete analysis (pdb_format dcd pdb etc...) 
#                           FMCSC_CPROGINDSTART=1, #starting snapshot 
#                           FMCSC_CMAXRAD=1.3, #clustering
#                           FMCSC_CRADIUS=0.1,
#                           FMCSC_CPROGINDRMAX=50, #search att
#                           FMCSC_CCUTOFF=10000) 

adjl <- CampaRi::mst_from_trj(trj = dtm,
                       distance_method = 5, clu_radius = 0.1,
                       birch_clu = F, mode = "fortran", rootmax_rad = 1.3,
                       tree_height = 5, n_search_attempts = 50, mute_fortran = F)

# x <- as.matrix(dtm)
# # print(str(x))
# print(x)
# print(dim(x))
# nr <- nrow(x)
# nc <- ncol(x)
# print(nr)
# print(nc)
# out_dr <- array(0.0, dim = c(nr, nr))
# # attr(out_dr, "Csingle") <- TRUE # in R the standard is double precision!!!
# # attr(x, "Csingle") <- TRUE
# diag <- 1
# method <- 1
# output <- .Fortran("dir_distance_f",
#                    x=x,
#                    nr=as.integer(nr),
#                    nc=as.integer(nc),
#                    diag=as.integer(diag),
#                    met=as.integer(method),
#                    d_out=out_dr)
