program alato

  use, intrinsic :: iso_c_binding

  implicit none

  integer :: nr = 6
  integer :: nc = 4
  integer :: diag = 1
  integer :: disc_met = 1
  integer :: ent_met = 0
  integer :: nbins = 3
  real(8) :: p_in = 3.2
  integer :: met = 10 ! 1 - C euclidean, 9 - Fortran euclidean
  integer i

  real(8), allocatable :: d_out(:, :)
  real(8), allocatable :: x(:, :)

  print *, "Starting test for distance functions!"
  print *, "Dataset shape (nr, nc): ", nr, nc

  allocate(d_out(nr, nr))
  allocate(x(nr, nc))

  call RANDOM_NUMBER(x)
  d_out = 0.0
  print *, x(1,:)
  print *, x(2,:)

  call dir_distance_f(x, nr, nc, diag, met, disc_met, ent_met, nbins, p_in, d_out)

  do i = 0, min(4, nr), 1
    if(i == 0) then
      write(*,*) "    ", 0, "           col1                      col2                       col3                       col4"
    else
      write(*,*) "row ", i, ":", d_out(i,1:4)
    end if
  end do
  print *, "Finished successfully!"
  deallocate(d_out)
  deallocate(x)

end program alato
