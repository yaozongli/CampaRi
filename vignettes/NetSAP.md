---
title: "NetSAP"
author: "Davide Garolini"
date: "14 Jan 2019"
vignette: >
  %\VignetteEngine{knitr::knitr}
  %\VignetteIndexEntry{NetSAP_vignette}
  %\usepackage[utf8]{inputenc}
---



This tutorial is focused on the application of NetSAP on a toy model. We build the model in the following way

## MST
We will use here the trajectory generated using the following command (needs original campari package):

```r
cat("STARTING VIGNETTE NetSAP_vignette...\n")
```

```
## STARTING VIGNETTE NetSAP_vignette...
```

```r
library(CampaRi); library(data.table)
```

```
## 
## Attaching package: 'data.table'
```

```
## The following objects are masked from 'package:CampaRi':
## 
##     dcast, melt
```

```r
# creating the oscillatory system - 3 states
tot_time <- 10000
ttt <- 1:tot_time
whn <- sample(ttt, size = 15)
wht <- sample(0:3, size = 16, replace = T)
tjum <- c(0, sort(whn), tot_time)
diff_tjum <- diff(tjum)
mat_fin <- array(0, dim = c(tot_time, 3))
annot <- array(NA, dim = tot_time)
for(i in 1:length(wht)){
  wherr <- (tjum[i]+1):tjum[i+1]
  if(wht[i]==1){
    mat_fin[wherr, 1] <- sin(wherr/5 + 0.7)
    mat_fin[wherr, 2] <- sin(wherr/5) + 0.2    
  } else if(wht[i]==2){
    mat_fin[wherr, 3] <- sin(wherr/5)
    mat_fin[wherr, 2] <- sin(wherr/5 + 0.5) + 0.2    
  } else if(wht[i]==3){
    mat_fin[wherr, 3] <- sin(wherr/5 + 0.35)
    mat_fin[wherr, 1] <- sin(wherr/5) + 0.2    
  }
  annot[wherr] <- rep(wht[i], diff_tjum[i]) 
}
mat_fin <- mat_fin + matrix(rnorm(tot_time*3)/5, nrow = tot_time, ncol = 3)
mat_fin <- .normalize(mat_fin)
plot_traces(t(mat_fin[250:500,]))
```

![plot of chunk toy_model](figure/toy_model-1.png)

```r
plot_traces(t(mat_fin))
```

![plot of chunk toy_model](figure/toy_model-2.png)

```r
plot(annot, pch = ".")
```

![plot of chunk toy_model](figure/toy_model-3.png)

```r
data.table::fwrite(as.data.frame(mat_fin), file = "test_trj.tsv", sep = " ")
```

Now we run a standard analysis to find the 4 clusters using the short spanning tree procedure to get the SAPPHIRE plot. Here no network inference is provided (to see all the passages, use `silent = F`).


```r
SST_campari(trj = as.matrix(mat_fin), pi.start = 1, silent = T)
sapphire_plot("PI_output_basename_pistart1.dat", timeline = T, ann_trace = annot)
```

```
## The maximum height of the y axis is: 8.517193 
## No option selected for the kind of annotation (annotation_type will be mainly used for the legend). 
##            Trying to guess.
```

![plot of chunk std_run](figure/std_run-1.png)

```r
out_bar2 <- nSBR("PI_output_basename_pistart1.dat", ny = 100, n.cluster = 4, plot = T)
```

```
## Reading PROGIDX file...
## Calculating the barrier statistics using 5  10  15  20  25  30  40  50  60  divisions for the MI ratio. 
## After 0 iteration we found 4 clusters.
```

![plot of chunk std_run](figure/std_run-2.png)

```r
out_sc2 <- score_sapphire("PI_output_basename_pistart1.dat", ann = as.numeric(annot + 1), manual_barriers = out_bar2$barriers_sel, plot_pred_true_resume = T)
```

```
## Reading PROGIDX file...
## You inserted manual barriers for testing.
## Having inserted the_sap we reorder the ann using it.
## We found roughly 29.8 % missassignment.
## Using adjusted_rand_index we obtained a final score of 0.4098328
```

![plot of chunk std_run](figure/std_run-3.png)

The NetSAP run, instead, should be able to unveil more clearly the involved states (to see all the passages, use `silent = F`).


```r
NetSAP(data_file = "test_trj.tsv", ncores_camp = 1, ncores_net = 1, folder = ".", trjsfile = "preproc_trj", base.name = "base_name",
       wii = 40, metho = "minkowski", princ_comps = 0, size_perc_search = 2000, qt = c(0.00015, 0.14),
       pi.start = 1, leaves.tofold = 5, search.attempts = 1000, state.width = 1000, print_mem = TRUE, silent = T)
```

```
## 
## Attaching package: 'pryr'
```

```
## The following object is masked from 'package:data.table':
## 
##     address
```

```
## 
## Attaching package: 'fastcluster'
```

```
## The following object is masked from 'package:stats':
## 
##     hclust
```

```
## 
```

```
## 
## Attaching package: 'WGCNA'
```

```
## The following object is masked from 'package:stats':
## 
##     cor
```

```r
sapphire_plot("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", timeline = T, ann_trace = annot)
```

```
## The maximum height of the y axis is: 8.517193 
## No option selected for the kind of annotation (annotation_type will be mainly used for the legend). 
##            Trying to guess.
```

![plot of chunk netsap_run](figure/netsap_run-1.png)

```r
out_bar <- nSBR("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", ny = 100, n.cluster = 4, plot = T)
```

```
## Reading PROGIDX file...
## Calculating the barrier statistics using 5  10  15  20  25  30  40  50  60  divisions for the MI ratio. 
## After 0 iteration we found 4 clusters.
```

![plot of chunk netsap_run](figure/netsap_run-2.png)

```r
out_sc1 <- score_sapphire("PI_output_base_name_pistart1_searchatt1000_leaves5_locut1000_PCA0.dat", ann = as.numeric(annot + 1), manual_barriers = out_bar$barriers_sel, plot_pred_true_resume = T)
```

```
## Reading PROGIDX file...
## You inserted manual barriers for testing.
## Having inserted the_sap we reorder the ann using it.
## We found roughly 17.86 % missassignment.
## Using adjusted_rand_index we obtained a final score of 0.8251499
```

![plot of chunk netsap_run](figure/netsap_run-3.png)



