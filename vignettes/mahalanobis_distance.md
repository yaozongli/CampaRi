---
title: "Mahalanobis distance"
author: "Davide Garolini"
date: "4 Feb 2019"
vignette: >
  %\VignetteEngine{knitr::knitr}
  %\VignetteIndexEntry{mahala_distance}
  %\usepackage[utf8]{inputenc}
---



## Finding the mahalanobis to enhance inter/intracluster distance

This vignette will show the minimum code to enhance the SAPPHIRE plot using the metric optimization algorithm.


```r
library(CampaRi)
######################################## test mahalanobis distance
# vars definitions
n_elem <- 700
n_feats <- 100

# random initiatialization (3 clusters)
c1 <- matrix(rnorm(n_elem*n_feats), nrow = n_elem, ncol = n_feats)
c2 <- matrix(rnorm(n_elem*n_feats), nrow = n_elem, ncol = n_feats)
c3 <- matrix(rnorm(n_elem*n_feats), nrow = n_elem, ncol = n_feats)

# shifts
c3[, 1:5] <- c3[, 1:5] + 5
c2[, 3:5] <- c2[, 3:5] - 3
c1[, 3:5] <- c1[, 3:5] - 3

# annotation of the true clusters
ann <- c(rep(1,n_elem*2), rep(2,n_elem))

# plot the PCA (one component will be enough to see the separation between one cluster and the other two)
library(ggfortify); library(ggplot2)
ggplot(data.frame(data = rbeta(100, shape1 = 0.5, shape2 = 0.5))) + geom_density(aes(x = data))
```

![plot of chunk mainrun](figure/mainrun-1.png)

```r
df <- cbind(as.data.frame(rbind(c1,c2,c3)), as.factor(c(rep("set1", n_elem), rep("set2", n_elem), rep("set3", n_elem))))
colnames(df)[ncol(df)] <- "tpl"
aplot <- prcomp(as.matrix(df[,1:(ncol(df)-1)]))
autoplot(aplot, data = df, colour = "tpl")
```

![plot of chunk mainrun](figure/mainrun-2.png)

```r
# find the distance matrix (Mahalonbis)
Xad <- CampaRi::find_mahaR(c1, c2, c3, K = 10, J = 5, tol = 10^(-5), C = 1, loss_f = "Hinge", silent = TRUE, verbose = FALSE, line_search_alpha = FALSE) # using linear programming
maha_like <- CampaRi::find_maha_likeRat(c1, c2, c3, silent = T) # using probability estimation

# create MST - maha
adjl <- mst_from_trj(trj = rbind(c1,c2,c3), distance_method = 12, dump_to_netcdf = T, metric_mat = Xad, return_tree_in_r = T, mute_fortran = T, silent = T)
ret <- gen_progindex(adjl, snap_start = 1, silent = T)
ret2 <- gen_annotation(ret, snap_start = 1, silent = T)
# create MST - maha - like
adjl <- mst_from_trj(trj = rbind(c1,c2,c3), distance_method = 12, dump_to_netcdf = T, metric_mat = maha_like, return_tree_in_r = T, mute_fortran = T, silent = T)
ret <- gen_progindex(adjl, snap_start = 3, silent = T)
ret2 <- gen_annotation(ret, snap_start = 3,  silent = T)
# create MST - euclidean
adjl <- mst_from_trj(trj = rbind(c1,c2,c3), distance_method = 5, return_tree_in_r = T, mute_fortran = T, silent = T)
ret <- gen_progindex(adjl, snap_start = 2, silent = T)
ret2 <- gen_annotation(ret, snap_start = 2, silent = T)
# plot SAPPHIRE
sapphire_plot(sap_file = "REPIX_000000000001.dat", ann_trace = ann, timeline = T, title = "Mahalanobis with dynamic programming") # should be better
```

```
## The maximum height of the y axis is: 6.956545 
## No option selected for the kind of annotation (annotation_type will be mainly used for the legend). 
##            Trying to guess.
```

![plot of chunk mainrun](figure/mainrun-3.png)

```r
sapphire_plot(sap_file = "REPIX_000000000003.dat", ann_trace = ann, timeline = T, title = "Mahalanobis with prob. estimation") # should be BETTER 2
```

```
## The maximum height of the y axis is: 6.956545 
## No option selected for the kind of annotation (annotation_type will be mainly used for the legend). 
##            Trying to guess.
```

![plot of chunk mainrun](figure/mainrun-4.png)

```r
sapphire_plot(sap_file = "REPIX_000000000002.dat", ann_trace = ann, timeline = T, title = "Euclidean") # 
```

```
## The maximum height of the y axis is: 6.956545 
## No option selected for the kind of annotation (annotation_type will be mainly used for the legend). 
##            Trying to guess.
```

![plot of chunk mainrun](figure/mainrun-5.png)


You can also plot what happened to the random point data-set using plotly:

```r
# plotting it
library(plotly)

# classical (first plot these, after the transformed points)
ap <- data.frame(t(c1))
bp <- data.frame(t(c2))
cp <- data.frame(t(c3))

# -----------------------------------------------------------------------------------
# transformed in the new metric space (it is not a correct action for the lack of rotations)
# let's transform the coordinate space 
p_transform <- sqrt(diag(Xad))
at <- sweep(x = c1, MARGIN = 1, STATS = p_transform, FUN = `*`)
bt <- sweep(x = c2, MARGIN = 1, STATS = p_transform, FUN = `*`)
ct <- sweep(x = c3, MARGIN = 1, STATS = p_transform, FUN = `*`)
# -----------------------------------------------------------------------------------

# plotting the points using plotly
df <- rbind(at,bt,ct)
the_max_influencers <- order(p_transform, decreasing = T) # taking the most relevant features
df <- as.data.frame(df)
names(df)[the_max_influencers[1]] <- 'x'
names(df)[the_max_influencers[2]] <- 'y'
names(df)[the_max_influencers[3]] <- 'z'
df <- cbind(df, cluster = c(rep('Cluster 1a', n_elem),rep('Cluster 1b', n_elem), rep('Cluster 2', n_elem)))

# df$cluster <- as.factor(df$cluster)

p <- plot_ly(df, x = ~x, y = ~y, z = ~z, color = ~cluster, colors = c('#BF382A', '#0C4B8E'), size = 0.1) %>%
  add_markers() %>%
  layout(scene = list(xaxis = list(title = 'x'),
                      yaxis = list(title = 'y'),
                      zaxis = list(title = 'z')))
p
```


