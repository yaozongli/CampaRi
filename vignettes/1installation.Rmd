---
title: "Installation"
date: "`r date()`"
vignette: >
  %\VignetteIndexEntry{"1Installation"}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(progress = FALSE)
```

# Complete installation procedure
Every line of code is available on the GitLab repository and therefore it can be installed using "devtools".
The package is tested on Ubuntu 16.04. 

* Full system Dependencies:
```{r, engine='bash', message=FALSE, warning=FALSE, eval=FALSE}
# ubuntu 16.04 (remember to have R installed -> sudo apt install r-base*)
sudo apt update
sudo apt install libnetcdff-dev # xenial: needed for light data handling
sudo apt install libarpack2-dev # xenial: needed for alternative to lapack spectral decomposition of matrices
sudo apt install libssl-dev # for devtools
sudo apt install libcurl4-gnutls-dev # for devtools
sudo apt install libtiff5-dev # for ClusterR
sudo apt install libgmp-dev # for ClusterR
# in the case of installation of original campari software: install_campari()
sudo apt install libfftw3-dev # for install_campari()

# some peculiarity for trusty as it had a different netcdf version and installation procedure
sudo apt-get install libnetcdff5 # trusty
sudo apt-get install libnetcdf-dev # trusty
sudo apt-get install libarpack2-dev # trusty

# osx # to-update
brew update
brew install netcdf --with-fortran
brew install arpack
# NOTE: also linux has a mirror of brew (called linuxbrew) which I personally
# suggest to install all the dependencies without need of sudo rights.

# IMPORTANT: as the number of package dependencies can overcome the std use
# the following enhancement of the number of DLLs

echo 'R_MAX_NUM_DLLS=500' >> ~/.Renviron
export R_MAX_NUM_DLLS=500  
```

* Bioconductor dependencies:
```{r message=FALSE, warning=FALSE, eval=FALSE}
source("http://bioconductor.org/biocLite.R")

biocLite(c("AnnotationDbi", "impute", "GO.db", "preprocessCore", "PairViz"))
install.packages("MESS", repos = "http://cran.us.r-project.org")
install.packages("MASS", repos = "http://cran.us.r-project.org")
install.packages("fields", repos = "http://cran.us.r-project.org")
install.packages("minerva", repos = "http://cran.us.r-project.org")
```

* Stable version: We will upload the package on CRAN as soon as possible.

* Development version: `devtools::install_git(url = "https://gitlab.com/CaflischLab/CampaRi")`

Please note that to unlock some analysis functions you need to install netcdf 4.1 or later library for backend fast data-handling.
This library must be installed --with-fortran bindings (it is possible to check this using the nc-config or nf-config commands (with --all option)).

The arpack library is optional and only needed for the Mahalanobis metric matrix extimation (not anymore).

## Installing the original version of CAMPARI

To do so it is enough to run the following code:

```{r install_campari, eval=TRUE, results=FALSE, message=FALSE}
devtools::install_git(url = "https://gitlab.com/CaflischLab/CampaRi")
library(CampaRi)
install_campari(install_ncminer = T, install_threads = T, silent_built = T, no_optimization = T)
```

```{r outsetup, include = FALSE, eval = T}
file.remove("../vignettes/VERSION")
file.remove("../vignettes/Makefile")
```


------------------------------------------------------------------------

### Original Manual

For more details, please refer to the main documentation of the original [CAMPARI documentation](http://campari.sourceforge.net/documentation.html).

### Question and issues

Please use [Gitlab issues](https://gitlab.com/CaflischLab/CampaRi/issues) if you want to file bug reports or feature requests.

------------------------------------------------------------------------

## Citation

Please cite the software and the SAPPHIRE algorithm paper:
* Davide Garolini, & Francesco Cocina. (2018, June 4). CampaRi: an R package for time series analysis (Version 0.9.1). Zenodo. http://doi.org/10.5281/zenodo.1260973
* Bloechliger, N., Vitalis, A. & Caflisch, A. A scalable algorithm to order and annotate continuous observations reveals the metastable states visited by dynamical systems. Comput. Phys. Commun. 184, 2446–2453 (2013).