#' @title Run the original Fortran code for sst SAPPHIRE
#' @description SST_campari calls the original CAMPARI executable to do the Short Spanning Tree (SST) and generate the SAPPHIRE table.
#'
#' @param trj Input trajectory (variables on the columns and equal-time spaced snpashots on the row). It must be a \code{matrix} or a \code{data.frame} of numeric.
#' @param camp_exe Character indicating the preferred executable location of CAMPARI-Fortran.
#' @param base_name Base name for output files.
#' @param sap_file Character indicating the output sapphire file name.
#' @param silent A logical value indicating whether the function has to remain silent or not. Default value is \code{FALSE}.
#' @param multi_threading  Should I do multi-threading (set also n_cores!)
#' @param rm_extra_files Logical which indicates if detailed files should be removed automatically (leaving only the main log and the SAPPHIRE table).  
#' @param n_cores  Number of physical cores for Fortran run
#' @param cmax_rad Radius for higher level of the tree clustering (hierarchically the largest clustering).
#' @param cradius Radius for the lowest level of the tree clusting.
#' @param percs Percentiles for estimating cmax_rad and cradius.
#' @param pcs Number of principal components if the PCA is desired as preprocessing. Done using CAMPARI-Fortran
#' @param state.width Width of the local cut. It is an optimization for multiple states scenario, where a kinetic function is needed.
#' @param cdistance Number which indicates the distance metric to be used. See details for details. Defaults to 7 (RMSD).
#' @param pi.start Starting point for the SAPPHIRE reordering of snapshots
#' @param search.attempts Number of trials to find closest neighbours in the clustering construction. The highest the better (the slower).
#' @param birchheight Height of the tree based clustering
#' @param leaves.tofold Number of leaves (points on the contour of the clusters) that will be contracted on the clostest neighbour.
#' @param ... These variables will go directly to \code{\link{run_campari}}
#'
#' @details For more details, please refer to the main documentation of the original campari software \url{http://campari.sourceforge.net/documentation.html}.
#'
#' @export SST_campari
#' @importFrom parallel detectCores


SST_campari <- function(trj, camp_exe = NULL, base_name = "basename", sap_file = NULL, silent = F, multi_threading = T, rm_extra_files = T, n_cores = NULL, 
                        cmax_rad = NULL, cradius = NULL, percs = NULL, pcs = NULL, state.width = NULL, cdistance = NULL, pi.start = NULL, search.attempts = NULL, birchheight = NULL,
                        leaves.tofold = NULL,
                        ...){
  
   n_cores_max <- parallel::detectCores()
   if(is.na(n_cores_max)) n_cores_max <- 1
   if(is.null(n_cores)) n_cores <- n_cores_max
  .isSingleInteger(n_cores, 1, n_cores_max)
  .isSingleLogical(silent)
  .isSingleLogical(multi_threading)
  .isSingleLogical(rm_extra_files)
  .isSingleChar(base_name)
  if(!is.null(sap_file)) .isSingleChar(sap_file)
  if(!is.null(camp_exe)) .isSingleChar(camp_exe)
  
  # Checking trajectory input
  if(!is.matrix(trj)){
    if(!is.data.frame(trj)) stop('trj input must be a matrix or a data.frame')
    trj <- as.matrix(trj)
  }
  nr <- nrow(trj)
  nc <- ncol(trj)
  
  
  if(is.null(cmax_rad) || is.null(cradius)){
    if(is.null(percs)) percs <- c(0.1, 0.9)
    nuuul <- sapply(percs, function(x) .isSingleNumeric(x, min = 0, max = 1))
    perc_points <- find_perc_TBC(trj, n_sampling = floor(nr / 100), perc = percs, silent = silent, ncore = n_cores)
    if(is.null(cmax_rad)) cmax_rad <- perc_points[2] 
    if(is.null(cradius)) cradius <- perc_points[1] 
  }
  if(is.null(cdistance)) cdistance <- 7
  if(is.null(pi.start)) pi.start <- 1
  if(is.null(state.width)) state.width <- floor(nr / 10)
  if(is.null(search.attempts)) search.attempts <- floor(nr / 20)
  if(is.null(leaves.tofold)) leaves.tofold <- 0
  if(is.null(birchheight)) birchheight <- 10
    
  .isSingleNumeric(cmax_rad, 0)
  .isSingleNumeric(cradius, 0)
  .isSingleInteger(cdistance, 1, 20)
  .isSingleInteger(pi.start, 1, nr)
  .isSingleInteger(search.attempts, 1, nr)
  .isSingleInteger(leaves.tofold, 0, nr/2)
  .isSingleInteger(state.width, 2, nr - 2)
  .isSingleInteger(birchheight, 2, nr - 2)
  if(!is.null(pcs)) .isSingleInteger(pcs, 1, nc - 1)
  
    
    
  if(is.null(pcs)){
    CampaRi::run_campari(trj = trj, base_name = base_name, campari_exe = camp_exe,         # file settings
                multi_threading = multi_threading, silent = silent, FMCSC_NRTHREADS=n_cores,       # multi-threading settings
                FMCSC_CPROGINDMODE=2,                         # SST = 2, MST = 1
                FMCSC_CCOLLECT=1,                             # if != 1 it makes subsampling
                FMCSC_CMODE=4,                                # main switcher for the clustering method and so on
                FMCSC_CDISTANCE=cdistance,                            # distance: RMSD without alignment is 7  (instead, dihedral distances need pdb_format dcd pdb etc...)
                FMCSC_CPROGINDSTART=pi.start,                 # starting snapshot
                FMCSC_CPROGINDRMAX=search.attempts,           # search attempts
                FMCSC_CPROGMSTFOLD=leaves.tofold,             # number of foldings to be done (of leaves)
                FMCSC_BIRCHHEIGHT=birchheight,                         # birch height
                FMCSC_CMAXRAD=cmax_rad,                # clustering top quantile
                FMCSC_CRADIUS=cradius,                # clustering low quantile
                FMCSC_CPROGRDEPTH=birchheight,                         # the depth define how many levels of the tree must be climbed to look for similar snapshots
                FMCSC_CPROGINDWIDTH=state.width, ...)              # local cut width should be a representative of the single state width
  }else{
    CampaRi::run_campari(trj = trj, base_name = base_name, campari_exe = camp_exe,         # file settings
                         multi_threading = multi_threading, silent = silent, FMCSC_NRTHREADS=n_cores,       # multi-threading settings
                         FMCSC_CPROGINDMODE=2,                         # SST = 2, MST = 1
                         FMCSC_CCOLLECT=1,                             # if != 1 it makes subsampling
                         FMCSC_CMODE=4,                                # main switcher for the clustering method and so on
                         FMCSC_PCAMODE=3,
                         FMCSC_CREDUCEDIM=pcs,
                         FMCSC_CDISTANCE=cdistance,                            # distance: RMSD without alignment is 7  (instead, dihedral distances need pdb_format dcd pdb etc...)
                         FMCSC_CPROGINDSTART=pi.start,                 # starting snapshot
                         FMCSC_CPROGINDRMAX=search.attempts,           # search attempts
                         FMCSC_CPROGMSTFOLD=leaves.tofold,             # number of foldings to be done (of leaves)
                         FMCSC_BIRCHHEIGHT=birchheight,                         # birch height
                         FMCSC_CMAXRAD=cmax_rad,                # clustering top quantile
                         FMCSC_CRADIUS=cradius,                # clustering low quantile
                         FMCSC_CPROGRDEPTH=birchheight,                         # the depth define how many levels of the tree must be climbed to look for similar snapshots
                         FMCSC_CPROGINDWIDTH=state.width, ...)              # local cut width should be a representative of the single state width
  }
  
  
  if(rm_extra_files) loop_file_rm(c(paste0(base_name, c('.key', '.in', '_END.int', '_END.pdb', '_START.int', '_START.pdb', '_VIS.vmd', '_traj.dcd')),
              'STRUCT_CLUSTERING.vmd', 'STRUCT_CLUSTERING.graphml', 'STRUCT_CLUSTERING.clu', 
              'FRAMES_NBL.nc', 'THREADS.log',
              'PRINCIPAL_COMPONENTS.evs', 'PRINCIPAL_COMPONENTS.dat'))
  thesap <- list.files()
  thesap <- thesap[grep(pattern = "PROGIDX_0000", x = thesap)]
  thesap <- thesap[grep(pattern = paste0("0", as.character(pi.start), ".dat"), x = thesap)]
  if(length(thesap) > 1 ) stop("Too many std PRGIDX files in the current folder. Please provide a folder with only one, just created PROGIDX.")
  if(is.null(sap_file)) sapfin <- paste0("PI_output_", base_name, "_pistart", as.character(pi.start), ".dat")
  else sapfin <- sap_file
  file.rename(from = thesap[1], to = sapfin)
  if(!silent) cat("SAP file:", sapfin, "\n")
}

#' @title Find indicative percentiles for tree based clustering in CAMPARI
#' @description Using bio3D and a random sampling of snapshots it calculates and indicative distribution of RMSD distances.
#'
#' @param trj Input trajectory (variables on the columns and equal-time spaced snpashots on the row). It must be a \code{matrix} or a \code{data.frame} of numeric.
#' @param perc Two numbers between 0 and 1 which are the percentile you desire.
#' @param var_weights Vector of global weights that can be applied to the selected
#' @param ncore number of cores for rmsd calculations (bio3D).
#' @param plot ploting the final distribution with the percentiles.
#' @param silent If \code{TRUE} everything will be silent.
#'
#' @details For more details, please refer to the main documentation of the original campari software \url{http://campari.sourceforge.net/documentation.html}.
#'
#' @return Two values for the lowest and higher percentile.
#' @examples
#' adjl <- find_perc_TBC(trj = matrix(rnorm(1000), nrow = 100, ncol = 10))
#'
#' @export find_perc_TBC

find_perc_TBC <- function(trj, perc = c(0.1, 0.9), var_weights = NULL, meth = "rmsd", n_sampling = 100, ncore = NULL, plot = F, silent = F){
  
  # checking the presence of bio3d
  .check_pkg_installation("bio3d")
  
  # standard variable checks
  .isSingleInteger(n_sampling)
  .isSingleLogical(silent)
  .isSingleChar(meth)
  if(.lt(perc) != 2) stop("perc vector must have only two entries.") 
  .isSingleNumeric(perc[1], 0, perc[2])  
  .isSingleNumeric(perc[2], perc[1], 1)  
  if(is.null(ncore)) ncore <- 1
  .isSingleInteger(ncore)
  if(ncore > parallel::detectCores() + 1) stop("Inserted too many cores in comparison to physical ones.")
  .checkM(meth, c("rmsd", "wasserstein"))
  
  # Checking trajectory input
  if(!is.matrix(trj)){
    if(!is.data.frame(trj)) stop('trj input must be a matrix or a data.frame')
    trj <- as.matrix(trj)
  }
  nr <- nrow(trj)
  nc <- ncol(trj)
  
  # checking the weights
  if(!is.null(var_weights)) null <- lapply(var_weights, .isSingleNumeric)
  if(!is.null(var_weights) && .lt(var_weights) != nc) stop("Number of columns (feats) and number of global weights differ.")
  
  # if(F){
  #   abc <- microbenchmark::microbenchmark(
  #     b = apply(a, 1, function(x) x*var_weights),
  #     c = t(var_weights*t(a)))
  #   autoplot(abc)
  # }
  
  which_to_test <- sample(1:nr, size = n_sampling)
  a <- trj[which_to_test, ]
  
  # if(!is.null(var_weights)) a <- apply(a, 1, function(x) x*var_weights)
  if(!is.null(var_weights)) a <- t(var_weights*t(a))
  
  if(!silent) cat('Discovering the just percentiles...\n')
  if(meth == "rmsd"){
    rmsd1 <- bio3d::rmsd(a = as.matrix(a), ncore = ncore, a.inds = 1:ncol(a))
  } else if(meth == "wasserstein"){
    nnn <- nrow(a)
    rmsd1 <- array(0.0, dim = c(nnn, nnn))
    for(i in 1:nnn)
      for(j in 1:nnn)
        rmsd1[i,j] <- .wasserstein1d(a[i,], a[j,], p = 2)
  }
  rmsd1 <- rmsd1[lower.tri(rmsd1, diag = FALSE)]
  dimmi_quanto <- quantile(rmsd1, probs = perc)
  
  if(!silent) cat('Found the following percentiles:', dimmi_quanto, '\n')
  
  if(plot){
    df <- data.frame("RMSDs" = rmsd1)  
    gp <- ggplot(df) + geom_density(aes(x = rmsd1)) + theme_classic() + geom_vline(xintercept = dimmi_quanto[1]) + geom_vline(xintercept = dimmi_quanto[2]) + xlab("RMSD distribution")
    print(gp)
  }
  
  # print(mem_used())
  invisible(dimmi_quanto)
}


.wasserstein1d <- function(a, b, p=1, wa=NULL, wb=NULL) {  	# wrong
  m <- length(a)
  n <- length(b)
  stopifnot(m > 0 && n > 0)
  if (m == n && is.null(wa) && is.null(wb)) {
    return(mean(abs(sort(b)-sort(a))^p)^(1/p))
  }
  if (is.null(wa)) {wa <- rep(1,m)}
  if (is.null(wb)) {wb <- rep(1,n)}
  stopifnot(length(wa) == m && length(wb) == n)
  ua <- (wa/sum(wa))[-m]
  ub <- (wb/sum(wb))[-n]
  cua <- c(cumsum(ua))  
  cub <- c(cumsum(ub))  
  temp <- cut(cub,breaks=c(-Inf,cua,Inf))
  arep <- table(temp) + 1  
  temp <- cut(cua,breaks=c(-Inf,cub,Inf))
  brep <- table(temp) + 1
  # we sum over rectangles with cuts on the vertical axis each time one of the two ecdfs makes a jump
  # xrep and yrep tell us how many times each of the x and y data have to be repeated in order to get the points on the horizontal axis
  # note that sum(xrep)+sum(yrep) = m+n-1 (we do not count the height-zero final rectangle where both ecdfs jump to 1)
  
  aa <- rep(sort(a), times=arep)
  bb <- rep(sort(b), times=brep)
  
  uu <- sort(c(cua,cub))
  uu0 <- c(0,uu)
  uu1 <- c(uu,1)
  areap <- sum((uu1-uu0)*abs(bb-aa)^p)^(1/p)
  #  print(rbind(uu1-uu0, pmax(aa,bb)-pmin(aa,bb)))
  return(areap)
}

# NOT REALLY working
# .wasserstein_gauss <- function(a, b){
#   
#   ## Not run: 
#   ## A simple Fortran example - n and x: assumed-size vector
#   code <- "
#       real hlp2,hlp,val_t1,val_t2
#       real why1, why2
#       integer i
#       why1 = 0
#       why2 = 0
#       norm = 1.0/(calcsz+1)
#       do 1 i=0,calcsz
#     1 why1 = why1 + veci(i)
#       do 2 i=0,calcsz
#     2 why2 = why2 + vecj(i)
#       val_t1 = norm*why1
#       val_t2 = norm*why2
#       why1 = 0
#       why2 = 0
#       do 3 i=0,calcsz
#     3 why1 = why1 + (veci(i)-val_t1**2)
#       do 4 i=0,calcsz
#     4 why2 = why2 + (vecj(i)-val_t2**2)
#       hlp = sqrt(norm*why1)
#       hlp2 = sqrt(norm*why2)
# 
#       val = val + abs(val_t1 - val_t2) + hlp + hlp2 + 2*hlp*hlp2
# "
#   thefn <- inline::cfunction(signature(calcsz="integer", veci="numeric", vecj="numeric", val="double"), 
#                              dim = c("", "calcsz", "calcsz", ""), code, convention=".Fortran")
#   thefn(veci = a, vecj = b, calcsz = length(a), val = 0)
# }
# 

