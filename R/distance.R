#' @title Distance routine similar to the standard dist
#' @description
#'      Function to calculate the distance matrix using fortran wrapper of C code. This function is mainly used for development.
#'      
#' @param x Data matrix. The result will have nrow x nrow dimension.
#' @param method One between c("euclidean", "euclidean_opt", "euclidean_mag", "maximum", "manhattan", "canberra", "binary", "minkowski", "euclidean_for", "mutual_info").
#' @param diag Logical. Should the diagonal also be used?
#' @param p Integer for the Minkowskij distance
#' @param disc_met discretization method for the mutual information estimation.
#' @param nbins number of bins for the discretization
#' 
#' @export distance
#' @useDynLib CampaRi, .registration = TRUE


# subroutine dir_distance_f(x, nr, nc, diag, met, d_out)
# ClusterR::distance_matrix()


distance <- function(x, method = "euclidean", diag = FALSE, p = 2, disc_met = "empiric", nbins = NULL) {
  # if(!is.na(pmatch(method, "euclidian"))) method <- "euclidean"
  METHODS <- c("euclidean", "euclidean_opt", "euclidean_mag", "maximum", "manhattan", "canberra", 
               "binary", "minkowski", "euclidean_for", "mutual_info")
  method <- .checkM(method, METHODS)
  .isSingleLogical(diag)
  .isSingleInteger(p)
  if(diag) diag = 1 else diag = 0
  
  x <- as.matrix(x)
  nr <- nrow(x)
  nc <- ncol(x)
  out_dr <- array(0.0, dim = c(nr, nr))
  # attr(out_dr, "Csingle") <- TRUE # in R the standard is double precision!!!
  # attr(x, "Csingle") <- TRUE
  
  # case for mutual_info
  if(method == 10){
    if(is.null(nbins)) nbins <- min(max(nr / 100, 2), nr - 1) # it must be more than 5 and less than  nr-1
    if(!.isSingleInteger(nbins)) stop("nbins must be a single integer")
    if(nbins < 2 || nbins > nr-1) stop("nbins must be more than 1 and less than nrows")
    disc_mets <- c("empiric")
    disc_met <- pmatch(disc_met, disc_mets)
    if(is.na(disc_met)) stop("invalid distance method")
    if(disc_met == -1) stop("ambiguous distance method")
    ent_met <- 0
  }else{
    nbins <- 10
    disc_met <- 1
    ent_met <- 0
  }
  
  # ! VARIABLES DEFINITIONS
  # ! x - matrix
  # ! nr - nrows
  # ! nc - ncols
  # ! diag - 1 if the diag is considered
  # ! met - main dist method
  # ! disc_met - method for discretization of the data set
  # ! ent_met - entropy type (method)
  # ! nbins - number of bins for histogram discretization
  # ! p_in - p value for Minkowski distance
  # ! d_out - output data set
  
  output <- .Fortran("dir_distance_f", PACKAGE="CampaRi",
                     x=x,
                     nr=as.integer(nr),
                     nc=as.integer(nc),
                     diag=as.integer(diag),
                     met=as.integer(method),
                     disc_met=as.integer(disc_met),
                     ent_met=as.integer(ent_met),
                     nbins=as.integer(nbins),
                     p_in=p,
                     d_out=out_dr)
  return(output)
}

.discretize <- function(x, numBins, r=range(x)){
  b = seq(from=r[1], to=r[2], length.out=numBins+1)
  y = table(cut(x, breaks=b , include.lowest=TRUE))
  
  return(y)
}
